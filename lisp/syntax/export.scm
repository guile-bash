;;; export.scm ---

;; Copyright (C) 2014  <Dmitry Bogatov <KAction@gnu.org>>

;; Author:  <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
(define-module (syntax export)
  #:export (with-export!))
(use-modules (srfi srfi-1))
(eval-when
 [compile load eval]
 (define (current-bindings)
   (let ((result '()))
     (module-for-each
      (lambda (key _)
	(set! result (cons key result)))
      (current-module))
     result)))

(define-syntax with-export!
  (lambda (x)
    (syntax-case x ()
      ((_ body ...)
       (with-syntax ((before-bindings (datum->syntax x (gensym))))
	 #'(begin
	     (eval-when (eval load compile expand)
	       (define before-bindings (current-bindings)))
	     body ...
	     (eval-when (eval load compile expand)
	       (let ((difference
		      (lset-difference eq? (current-bindings)
				       before-bindings (list 'before-bindings))))
		 (call-with-deferred-observers
		  (lambda ()
		    (module-replace! (current-module) difference)))))))))))
