;;; record.scm ---

;; Copyright (C) 2014  <Dmitry Bogatov <KAction@gnu.org>>

;; Author:  <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
(define-module (syntax record)
  #:export (define-record-type*))
(use-modules (srfi srfi-1))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-26))

(eval-when
 (compile load eval)
 (define (make-rewrite pattern)
   (lambda args
     (string->symbol (apply format #f pattern args))))
 (define-public record-name (make-rewrite "<~s>"))
 (define-public constructor-name (make-rewrite "make-~s"))
 (define-public constructor-name* (make-rewrite "make-~s*"))
 (define-public predicate-name (make-rewrite "~s?"))
 (define-public accessor-name (make-rewrite "~s-~s"))
 (define-public setter-name (make-rewrite "set-~s-~s!")))

(define-syntax define-record-type*
  (lambda (x)
    (syntax-case x ()
      ((_ name (field ...))
       (and (identifier? #'name)
	    (every identifier? #'(field ...)))
       (let ((<$> (lambda (f syn)
		    (datum->syntax x (f (syntax->datum syn)))))
	     (<$$> (lambda (f s1 s2)
		     (datum->syntax x (f (syntax->datum s1)
					 (syntax->datum s2))))))
	 (with-syntax
	     ((constructor  (<$> constructor-name #'name))
	      (constructor* (<$> constructor-name* #'name))
	      (predicate    (<$> predicate-name #'name))
	      (recname      (<$> record-name #'name))
	      ((accessor ...) (map (cut <$$> accessor-name #'name <>)
				   #'(field ...)))
	      ((setter ...)   (map (cut <$$> setter-name #'name <>)
				   #'(field ...))))
	   #'(begin
	       (define-record-type recname
		 (constructor field ...)
		 predicate
		 (field accessor setter) ...)
	       (define* (constructor* #:key field ...)
		 (constructor field ...)))))))))
