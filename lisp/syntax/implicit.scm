;;; implicit.scm ---

;; Copyright (C) 2014  <Dmitry Bogatov <KAction@gnu.org>>

;; Author:  <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
(define-module (syntax implicit)
  #:export (define-syntax-cases
	     define-transformer
	     %syntax
	     %datum)
  #:replace ((datum->syntax* . datum->syntax)
	     (with-syntax* . with-syntax)))
(use-modules (srfi srfi-1))

(eval-when (eval load compile)
  (define *current-syntax* (make-parameter #'dummy-syntax-object))
  (define *current-outer-syntax* (make-parameter #'dummy-syntax-object)))

(define-syntax %syntax (const #'(*current-syntax*)))
(define-syntax %datum (const #'(syntax->datum (*current-syntax*))))

(define datum->syntax*
  (case-lambda
   [(datum)     (datum->syntax (*current-outer-syntax*) datum)]
   [(for datum) (datum->syntax for datum)]))

(define-syntax define-syntax-cases
  (lambda (x)
    (syntax-case x ()
      [(_ (name kw ...) clause ...)
       (and (identifier? #'name)
	    (every identifier? #'(kw ...)))
       #'(define-syntax name
	   (lambda (env)
	     (parameterize ([*current-outer-syntax* env]
			    [*current-syntax* env])
               (syntax-case env (kw ...) clause ...))))])))

(define-syntax-cases (define-transformer)
  [(_ name body ...)
   (identifier? #'name)
   #'(eval-when (expand compile load eval)
       (define (name env)
	 (parameterize ([*current-syntax* env])
	   body ...)))]
  [(_ (name kw ...) clause ...)
   (and (identifier? #'name)
	(every identifier? #'(kw ...)))
   #'(eval-when (expand compile load eval)
       (define (name env)
	 (parameterize ([*current-syntax* env])
	   (syntax-case env (kw ...) clause ...))))])

(define-syntax-cases (with-syntax*)
  [(_ () body ...)
   #'(with-syntax () body ...)]
  [(_ (bind rest ...) body ...)
   (if (identifier? #'bind)
       #'(with-syntax ((bind bind))
	   (with-syntax* (rest ...) body ...))
       #'(with-syntax (bind)
	   (with-syntax* (rest ...) body ...)))])

(define-public (fmap f . args)
  (datum->syntax* (apply f (map syntax->datum args))))
