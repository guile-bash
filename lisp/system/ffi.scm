;;; ffi.scm ---

;; Copyright (C) 2014  <Dmitry Bogatov <KAction@gnu.org>>

;; Author:  <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This module defines structure <ffi-object> describing scheme
;; representation of C datatype. It contains of following fields:
;;
;;  * type, see (system foreign)
;;  * encoder, that convert Scheme object to C datatype
;;  * decoder, that convert C datatype to Scheme object
;;  * size, for pointer types.
;;
;;  Either encoder or decoder, but not both can be #f, this means
;;  than operation is not supported. If both are supported,
;;       decoder . encoder == id
;;  Reverse is not necessary correct, since decoding can omit some
;;  details of original object.
;;
;;  This object is used by macros in (syntax ffi)
;;
;;  Also, this module provides set of macros to define <ffi>s
;;  for common datatypes, encountered in C.
;;
;;  The most generic form is `define-ffi-object`, that is little more,
;;  than keyword wrapper around <ffi-object> constructor. It have form
;;
;;      (define-ffi-object object:
;;          #:decoder pointer->object
;;          #:encoder object->pointer
;;          #:type '* ;; Not needed, pointer is default)
;;
;;  It is recommended, that if type is '*, to name decoders and encoders
;;  like in example, and if type is integral, use integral->object and
;;  object->integral naming. By convention name of <ffi> variable
;;  ends with colon.
;;
;;  If type is '*, `define-ffi-object` also defines `?object:` variable,
;;  that in addition can convert #f <-> %null-pointer.
;;
;;  Second provided macro is `define-ffi-enum`. It is called in form
;;      (define-ffi-enum enum/foo: (empty (half 75) full))
;;  which corresponds to following C declaration
;;      enum foo { empty, half = 75, full };
;;  Generated encoders/decoders converts symbols to/from int.
;;
;;  Next, macro than corresponds to bitmasks, `define-ffi-mask`. Example:
;;  (define-ffi-mask mask/bar:
;;      ((nice #x1) (bad #x3))
;;      #:type int8)
;;
;;  Next, dealing with structs. For example, in C
;;      struct foo_struct { int a; char *s; int i_do_not_care; };
;;  can be directly translated with `define-ffi-struct`
;;  (define-ffi-struct foo: ((int a) (?string: s) int))
;;
;;  It defines srfi-9 struct <foo> with fields `a' and `b'.  If not
;;  every field given name in `define-ffi-struct`, during decoding
;;  such fields are ignored, and during encoding are filled with
;;  specially-prepared garbage.
;;
;;  Not very often, but we need to deal with function pointers.  It
;;  can be nicely handled with
;;      (define-ffi-func func/make: ((string: name) (string: value) -> void))
;;
;;  The most annoying part of C from ffi part of view are unions,
;;  since they cannot be parsed without context. Your best bet is
;;  define dummy struct of needed size in place of union, and count
;;  offsets yourself.
;;
;;; Code:

(define-module (system ffi)
  #:replace (equal?)
  #:export (guard)
  #:export (*: void: ?*: string: ?string:)
  #:export (call-with-provided-memory)
  #:export (define-extern)
  #:export (define-ffi)
  #:export (define-ffi-enum)
  #:export (define-ffi-func)
  #:export (define-ffi-mask)
  #:export (define-ffi-object)
  #:export (define-ffi-struct)
  #:export (extern)
  #:export (extern-pointer)
  #:export (extern-pointer-array)
  #:export (make-ffi-enum)
  #:export (make-ffi-func-decoder)
  #:export (make-ffi-func-encoder)
  #:export (make-ffi-make)
  #:export (?pointer->string ?string->pointer)
  #:export (make-ffi-struct)
  #:export (offset-of)
  #:export (size-of)
  #:export (unsafe)
  #:export (with-provided-memory)
  #:export (c-free c-malloc c-memmove c-strdup c-strlen))

(use-modules (ice-9 control))
(use-modules (ice-9 q))
(use-modules (oop goops))
(use-modules (ice-9 decorate))
(use-modules (ice-9 curried-definitions))
(use-modules (ice-9 match))
(use-modules (srfi srfi-9))
(use-modules (ice-9 optargs))
(use-modules (rnrs bytevectors))
(use-modules (srfi srfi-1))
(use-modules (syntax export))
(use-modules (syntax functional))
(use-modules (syntax implicit))
(use-modules (syntax record))
(use-modules (system foreign))
(use-modules (texinfo string-utils))

;; Many primitive types have identity as encoder and/or decoder,
;; but to make procedure names more meaningful, each one receive
;; it's own instance of identity function.
(define (make-identity)
  (lambda (x) x))


(define-class <ffi-object> ()
  (ffi-decoder #:init-keyword #:decoder #:accessor .ffi-decoder
	       #:init-form (cute error "No decoder" <...>))
  (ffi-encoder #:init-keyword #:encoder #:accessor .ffi-encoder
	       #:init-form (cute error "No encoder" <...>))
  (ffi-type    #:init-keyword #:type    #:accessor .ffi-type
	       #:init-value '*)
  (ffi-clone   #:init-keyword #:clone   #:accessor .ffi-clone
	       #:init-form (make-identity))
  (ffi-free    #:init-keyword #:free    #:accessor .ffi-free
	       #:init-form (make-identity)))
(define guard (make-guardian))

(define-record-type <unsafe>
  (unsafe value)
  unsafe?
  (value unsafe-coerce))

(define-syntax-rule (promise-overload method-name)
  (define-method (method-name (promise <promise>) . args)
    (apply method-name (force promise) args)))
(promise-overload .ffi-decoder)
(promise-overload .ffi-encoder)
(promise-overload .ffi-type)
(promise-overload .ffi-clone)
(promise-overload .ffi-free)

(define (force* x)
  (if (promise? x)
      (force x)
    x))

(define ((use-ffi-accessor accessor) obj value)
  ((accessor obj) value))
(define clone (use-ffi-accessor .ffi-clone))
(define free  (use-ffi-accessor .ffi-free))

(define* (decode ffi-obj c-value #:key free-after?)
  (define scm-value ((.ffi-decoder ffi-obj) c-value))
  (when free-after?
    (free ffi-obj c-value))
  scm-value)

(define* (encode ffi-obj scm-value #:key (clone-after? #t))
  (if (unsafe? scm-value)
      (unsafe-coerce scm-value)
    (let ((c-value ((.ffi-encoder ffi-obj) scm-value)))
      ;; If scm-value is #f it means that we are encoding either
      ;; boolean or %null-pointer. In both cases cloning is not
      ;; needed, in second it is a problem, since clone function
      ;; expects valid pointer, not null one.
      (if (and scm-value clone-after?)
	  (clone ffi-obj c-value)
	c-value))))

(define-record-type* field (accessor ffi-object))
(define-class <ffi-struct> (<ffi-object>)
  (struct-class #:init-keyword #:struct-class #:accessor .struct-class)
  (ffi-objects  #:init-keyword #:ffi-objects))
(promise-overload .struct-class)
(define-method (.ffi-objects (obj <ffi-struct>))
  (map force* (slot-ref obj 'ffi-objects)))

(define-method (size-of (struct <ffi-struct>))
  (sizeof (map .ffi-type (.ffi-objects struct))))
(define-method (size-of x)
  (sizeof x))

(define-method (equal? x y)
  ((@ (guile) equal?) x y))

(define-method (equal? (this <object>) (other <object>))
  (let* ((class_ (class-of this))
	 (slots (map car (class-slots class_))))
    (define (slot-values instance)
      (map (cute slot-ref instance <>) slots))
    (if (eq? class_ (class-of other))
	(false-if-exception (every equal? (slot-values this) (slot-values other)))
      #f)))

(define-method (offset-of (struct <ffi-struct>) (field-name <symbol>))
  (define field-names (map car (class-slots (.struct-class struct))))
  (define ffi-objs (.ffi-objects struct))
  (unless (member field-name field-names)
    (error "Struct do not have such field:" struct field-name))
  (let* ((fields-before# (list-index (cute eq? field-name <>) field-names)))
    (if (zero? fields-before#)
	0
      (let* ((ffi-objects-before (take ffi-objs fields-before#))
	     (c-types-before      (map .ffi-type ffi-objects-before)))
	(sizeof c-types-before)))))

(define-method (offset-of (struct <ffi-struct>) (base <foreign>) (field-name <symbol>))
  (make-pointer (+ (pointer-address base) (offset-of struct field-name))))

(define decorate:null-decoder
  (make-decorator 'null-decoder ((f ptr))
		  (if (null-pointer? ptr)
		      #f
		    (f ptr))))

(define decorate:null-clone
  (make-decorator 'null-clone ((f ptr))
		  (if (null-pointer? ptr)
		      %null-pointer
		    (f ptr))))

(define decorate:null-encoder
  (make-decorator 'null-encoder ((f value))
		  (if value
		      (f value)
		    %null-pointer)))

(define (slot-modify obj slot fn)
  (slot-set! obj slot (fn (slot-ref obj slot))))

(define (force-ffi-objects ffi-struct)
  (slot-modify ffi-struct 'ffi-objects (cute map force* <>)))

(define (make-nullable obj)
  (define new (shallow-clone obj))
  (slot-modify new 'ffi-encoder decorate:null-encoder)
  (slot-modify new 'ffi-decoder decorate:null-decoder)
  (slot-modify new 'ffi-clone   decorate:null-clone)
  new)

;; A bit hacky way to get integer->foo functions on toplevel, allowing
;; to avoid some macros and simplifying others.
(define (toplevel-define-accessors ffi-obj-name)
  (define symbol (normalize-ffi-object-name ffi-obj-name))
  (define int/encoder-name (symbol-append symbol '->integer))
  (define int/decoder-name (symbol-append 'integer-> symbol))
  (define pointer/encoder-name (symbol-append symbol '->pointer))
  (define pointer/decoder-name (symbol-append 'pointer-> symbol))
  (define pointer/free-name (symbol-append 'free- symbol '-pointer))
  (define pointer/clone-name (symbol-append 'clone- symbol '-pointer))
  (define ffi-object (module-ref (current-module) ffi-obj-name))
  (define (current-module-define! symbol proc)
    (module-define! (current-module) symbol proc)
    (set-procedure-property! proc 'name symbol))
  (define (toplevel-define-field symbol field)
    (current-module-define! symbol (slot-ref ffi-object field)))
  (if (eq? '* (slot-ref ffi-object 'ffi-type))
      (begin
	(toplevel-define-field pointer/decoder-name 'ffi-decoder)
	(toplevel-define-field pointer/encoder-name 'ffi-encoder)
	(toplevel-define-field pointer/free-name    'ffi-free)
	(toplevel-define-field pointer/clone-name   'ffi-clone))
    (begin
      (toplevel-define-field int/decoder-name 'ffi-decoder)
      (toplevel-define-field int/encoder-name 'ffi-encoder))))

(define (string-last str)
  (string-ref str (1- (string-length str))))

(define (string-init str)
  (substring str 0 (1- (string-length str))))

(define (normalize-ffi-object-name name)
  ;; Just strip trailing colon, if any.
  ;; There should be one, really.
  (define name-str (symbol->string name))
  (unless (eqv? (string-last name-str) #\:)
    (error "Wrong ffi object name (should end with colon):" name))
  (string->symbol (string-init name-str)))

(define (toplevel-maybe-define-nullable name)
  (define object (module-ref (current-module) name))
  (define nullable-name (symbol-append '? name))
  (when (eq? '* (.ffi-type object))
    (module-define! (current-module) nullable-name
		    (make-nullable object))
    (toplevel-define-accessors nullable-name)))

(define-syntax-rule (define-ffi-object name kw ...)
  (begin
    (define name (make <ffi-object> kw ...))
    (toplevel-define-accessors 'name)
    (toplevel-maybe-define-nullable 'name)))

(define ((enum-lambda alist) key)
  (or (assq-ref alist key)
      (error "enum-lambda: key not found:" alist key)))

(define (regenerate-enum-values lst)
  (let ((prev-value -1))
    (define (regenerate x)
      (match x
	((key value)   (cons key value))
	((key . value) (cons key value))
	(key           (let ((value (1+ prev-value)))
			 (set! prev-value value)
			 (cons key value)))))
    (map-in-order regenerate lst)))

(define (reverse-cons pair)
  (call-with-values (cute car+cdr pair) xcons))

(define* (make-ffi-enum alist #:key (type int))
  (let ((full-form-alist (regenerate-enum-values alist)))
    (make <ffi-object>
      #:encoder (enum-lambda  full-form-alist)
      #:decoder (enum-lambda (map reverse-cons full-form-alist))
      #:type type)))

(define-syntax-rule (define-ffi-enum name (clause ...) kw ...)
  (begin
    (define name (make-ffi-enum '(clause ...) kw ...))
    (toplevel-define-accessors 'name)))

(define* (make-ffi-mask alist #:key (type int))
  (define mask-values (map cdr alist))
  (define key->value (enum-lambda alist))
  (define value->key (enum-lambda (map reverse-cons alist)))
  (define (encoder symbols)
    (apply logior (map key->value symbols)))
  (define (decoder val)
    (define (mask-present? mask)
      (eqv? mask (logand mask val)))
    (map value->key (filter mask-present? mask-values)))
  (make <ffi-object>
    #:encoder encoder
    #:decoder decoder
    #:type type))

(define-syntax-rule (define-ffi-mask name ((key value) ...) kw ...)
  (begin
    (define name (make-ffi-mask '((key . value) ...) kw ...))
    (toplevel-define-accessors 'name)))

(define (call-with-provided-memory ffi-structs producer consumer)
  (define (make-vector-for-ffi st)
    (make-bytevector (slot-ref st 'struct-size)))
  (let* ((vectors  (map make-vector-for-ffi ffi-structs))
	 (pointers (map bytevector->pointer vectors))
	 (retvalue (apply producer pointers)))
    (apply consumer retvalue (map decode ffi-structs pointers))))

(define-syntax with-provided-memory
  (syntax-rules (=)
    ((_ ((ffi-obj name) ...) (result = memaction) exp exp* ...)
     (call-with-provided-memory
      (list ffi-obj ...)
      (lambda (name ...) memaction)
      (lambda (result name ...) exp exp* ...)))))


;; function->pointer transformation


;; Every arg-spec is cons pair (free-after? . ffi-object)
(define ((make-ffi-func-encoder ret-obj clone-retval? arg-specs) scm-fn)
  (define (proxy . c-value-args)
    (define (decode-and-maybe-free arg-spec c-value)
      (decode (cdr arg-spec) c-value #:free-after? (car arg-spec)))
    (define scm-value-args (map decode-and-maybe-free arg-specs c-value-args))
    (define scm-ret-value (apply scm-fn scm-value-args))
    (define c-ret-value (encode ret-obj scm-ret-value #:clone-after? clone-retval?))
    c-ret-value)
  (define (arg-spec-ffi-type arg-spec)
    (.ffi-type (cdr arg-spec)))
  (procedure->pointer (slot-ref ret-obj 'ffi-type)
		      proxy
		      (map arg-spec-ffi-type arg-specs)))

(define-syntax unify-encode-clause
  (syntax-rules ()
    ((_ (#:const ffi-obj _ ...)) (cons #f ffi-obj))
    ((_ (#:alloc ffi-obj _ ...)) (cons #t ffi-obj))
    ((_ (ffi-obj _ ...))         (cons #f ffi-obj))
    ((_ ffi-obj)                 (cons #f ffi-obj))))

(define-syntax define-ffi-func
  (syntax-rules (->)
    ((_ name (arg ... -> #:const ret-obj))
     (define-ffi-object name
       #:decoder unsafe
       #:encoder (make-ffi-func-encoder
		  ret-obj #f
		  (list (unify-encode-clause arg) ...))))
    ((_ name (arg ... -> #:alloc ret-obj))
     (define-ffi-object name
       #:decoder unsafe
       #:encoder (make-ffi-func-encoder
		  ret-obj #f
		  (list (unify-encode-clause arg) ...))))
    ((_ name (arg ... -> ret-obj))
     (define-ffi-func name (arg ... -> #:const ret-obj)))))


;; pointer->function transformation.

(define (make-raw-procedure ret-obj args-objs foreign-address)
  (pointer->procedure (.ffi-type ret-obj) foreign-address
		      (map .ffi-type args-objs)))

(define (make-c-values arg-specs scm-args provided-args)
  (define (xenq! el q) (enq! q el))
  (define scm-args-q (fold xenq! (make-q) scm-args))
  (define provided-args-q (fold xenq! (make-q) provided-args))
  (define (process-arg-spec arg-spec)
    (match arg-spec
      (('fix ffi-obj value) value)
      (('provide ffi-obj)   (deq! provided-args-q))
      (('const  ffi-obj)    (encode ffi-obj (deq! scm-args-q) #:clone-after? #f))
      (('normal ffi-obj)    (encode ffi-obj (deq! scm-args-q) #:clone-after? #t))))
  (define c-values (map process-arg-spec arg-specs))
  (unless (q-empty? scm-args-q)
    (error "Wrong number of arguments (too much):" (car scm-args-q)))
  c-values)

;; Every element of ARG-SPECS is expected to be a list, describing
;; argument of function. Here is possibilities:
;;
;; (fix <ffi-object> <fixed-value>)
;; (normal <ffi-object>)
;; (const <ffi-object>)
;; (provide <ffi-object)
(define ((make-ffi-func-decoder ret-obj free-retval? arg-specs) foreign-address)
  (define arg-spec-ffi-object second)
  (define (arg-spec-type-provide? arg-spec)
    (eq? (car arg-spec) 'provide))
  (define args-ffi-objs (map arg-spec-ffi-object arg-specs))
  (define raw-procedure (make-raw-procedure ret-obj args-ffi-objs foreign-address))
  (define provide-ffi-objs (filter arg-spec-type-provide? arg-specs))
  (lambda scm-args
    (call-with-provided-memory
     provide-ffi-objs
     (lambda provided-pointers
       (define c-values (make-c-values arg-specs scm-args provided-pointers))
       (define c-ret-value (apply raw-procedure c-values))
       (decode ret-obj c-ret-value #:free-after? free-retval?))
     (lambda (scm-ret-value . provided-scm-values)
       (if (null? provided-scm-values)
	   scm-ret-value
	 (cons scm-ret-value provided-scm-values))))))



(define (bool->integer b)
  (if b 1 0))

(define (integer->bool value)
  (not (zero? value)))

(define-ffi-object void: #:decoder (const *unspecified*) #:type void)
(define-ffi-object *: #:decoder (make-identity) #:encoder (make-identity))
(define-ffi-object bool: #:decoder integer->bool #:encoder bool->integer #:type int)

(define (mirror-at-toplevel type-name)
  (define type (module-ref (current-module) type-name))
  (define varname (symbol-append type-name ':))
  (module-define! (current-module) varname
    (make <ffi-object> #:encoder (make-identity) #:decoder (make-identity) #:type type))
  (module-export! (current-module) (list varname)))

(for-each mirror-at-toplevel
  '(size_t int long ptrdiff_t int8 int16 int32 int64 uint8 uint16 uint32 uint64))

(define-syntax unify-decode-clause
  (syntax-rules (:=)
    ((_ (ffi-obj := value))        (list 'fix ffi-obj value))
    ((_ (#:alloc ffi-obj _ ...))   (list 'normal  ffi-obj))
    ((_ (#:const ffi-obj _ ...))   (list 'const   ffi-obj))
    ((_ (ffi-obj _ ...))           (list 'normal  ffi-obj))
    ((_ ffi-obj)                   (list 'normal  ffi-obj))
    ((_ (#:provide ffi-obj _ ...)) (list 'provide ffi-obj))))

(define (scm-symbol->c-symbol symb)
  (transform-string (symbol->string symb) #\- #\_))

(define* (dynamic-pointer* def-symbol #:key symbol (from (dynamic-link)))
  (define c-symbol-str (or symbol (scm-symbol->c-symbol def-symbol)))
  (dynamic-pointer c-symbol-str from))

(define (toplevel-define-ffi name ret-obj free-retval? arg-specs . kw)
  (define (make-procedure* ret*)
    (define make-decoder* (make-ffi-func-decoder ret* free-retval? arg-specs))
    (define foreign-pointer (false-if-exception (apply dynamic-pointer* name kw)))
    (when foreign-pointer
      (make-decoder* foreign-pointer)))
  (define (wrap-q-empty proc)
    (lambda args
      (catch 'q-empty (cute apply proc args)
	(lambda args (error "Not enough arguments:" name )))))

  (module-define! (current-module) name
    (wrap-q-empty (make-procedure* ret-obj)))
  (when (eq? '* (.ffi-type ret-obj))
    (module-define! (current-module) (symbol-append name '*)
      (wrap-q-empty (make-procedure* ?*:)))))

(define-syntax define-ffi
  (syntax-rules (-> ::)
    ((_ name (arg-clause ... -> free-retval? ret-obj) kw ...)
     (toplevel-define-ffi 'name ret-obj free-retval?
			  (list (unify-decode-clause arg-clause) ...)
			  kw ...))
    ((_ name (arg-clause ... -> #:const ret-obj) kw ...)
     (define-ffi name (arg-clause ... -> #f ret-obj) kw ...))
    ((_ name (arg-clause ... -> #:alloc ret-obj) kw ... )
     (define-ffi name (arg-clause ... -> #t ret-obj) kw ...))
    ((_ name (arg-clause ... -> ret-obj) kw ...)
     (define-ffi name (arg-clause ... -> #f ret-obj) kw ...))
    ((_ name :: #:const ret-obj kw ...)
     (define-ffi (-> #f ret-obj) kw ...))
    ((_ name :: #:alloc ret-obj kw ...)
     (define-ffi (-> #t ret-obj) kw ...))))

(define-ffi c-free (*: -> void:) #:symbol "free")
(define-ffi c-memmove ((*: dest) (*: src) (size_t: n) -> *:) #:symbol "memmove")
(define-ffi c-strdup (*: -> *:) #:symbol "strdup")
(define-ffi c-malloc (size_t: -> *:) #:symbol "malloc")

(define-ffi-object string:
  #:decoder pointer->string
  #:encoder string->pointer
  #:clone c-strdup
  #:free c-free)

(define (string-like->pointer obj)
  (string->pointer
   (let loop ((object obj))
     (let/ec return
       (when (string? object)
	 (return object))
       (when (symbol? object)
	 (return (loop (symbol->string object))))
       (when (keyword? object)
	 (return (loop (keyword->symbol object))))
       (scm-error 'wrong-type-arg "string-like->pointer"
		  "Unsupported value ~A" (list obj))))))

(define (pointer->string-like ptr)
  (string->symbol (pointer->string ptr)))

(export string-like:)
(define-ffi-object string-like:
  #:decoder pointer->string-like
  #:encoder string-like->pointer
  #:clone c-strdup
  #:free c-free)

(define (toplevel-define-scm-struct-class struct-name field-names)
  (define class-name (symbol-append '<struct- struct-name '>))
  (define (make-slot-spec field-name)
    (list field-name #:init-keyword (symbol->keyword field-name)))
  (define slot-specs (map make-slot-spec field-names))
  (define class_ (make <class>
		   #:dsupers (list <object>)
		   #:slots slot-specs
		   #:name class-name))
  (module-define! (current-module) class-name class_)
  class_)

(define (make-ffi-struct name fields-alist)
  (define ffi-objs    (map cdr fields-alist))
  (define field-names (map car fields-alist))
  (define struct-class_  (toplevel-define-scm-struct-class name field-names))
  (define c-types (delay (map .ffi-type ffi-objs)))

  (define (parse-c-values foreign-pointer)
    (parse-c-struct foreign-pointer (force c-types)))
  (define (%encoder struct)
    (define scm-values (map (cute slot-ref struct <>) field-names))
    (define c-values (map (cute encode <> <> #:clone-after? #f) ffi-objs scm-values))
    (make-c-struct (force c-types) c-values))
  (define (%decoder foreign-pointer)
    (define c-values (parse-c-values foreign-pointer))
    (define scm-values (map decode ffi-objs c-values))
    (define result-struct (make struct-class_))
    (map (cute slot-set! result-struct <> <>) field-names scm-values)
    result-struct)
  (define (%clone foreign-pointer)
    (define sizeof-struct (sizeof (force c-types)))
    (define c-values (parse-c-values foreign-pointer))
    (define cloned-c-values (map clone ffi-objs c-values))
    (define new-memory (c-malloc sizeof-struct))
    (when (null-pointer? new-memory)
      (error "No memory:" sizeof-struct))
    (c-memmove new-memory (make-c-struct (force c-types) c-values) sizeof-struct))
  (define (%free foreign-pointer)
    (define c-values (parse-c-values foreign-pointer))
    (for-each free ffi-objs c-values)
    (c-free foreign-pointer))
  (make <ffi-struct>
    #:encoder %encoder
    #:decoder %decoder
    #:free %free
    #:clone %clone
    #:type '*
    #:struct-class struct-class_
    #:ffi-objects ffi-objs))

(define-syntax-rule (define-ffi-struct name ((ffi-obj field-name) ...))
  (begin
    (define name (make-ffi-struct (normalize-ffi-object-name 'name)
				  (list (cons 'field-name (delay ffi-obj)) ...)))
    (toplevel-define-accessors 'name)
    (toplevel-maybe-define-nullable 'name)))


(define (parse-c-type ptr type)
  (car (parse-c-struct ptr (list type))))
(define (value-memory type value)
  (make-c-struct (list type) (list value)))

(define ((extern ffi-obj) pointer)
  (define objtype (.ffi-type ffi-obj))
  (define objsize (size-of objtype))
  (define (get)
    (decode ffi-obj (parse-c-type pointer objtype)))
  (define (set value)
    (c-memmove pointer (value-memory objtype (encode ffi-obj value)) objsize))
  (make-procedure-with-setter get set))

(define* (extern-pointer ffi-obj #:key (free-on-set? #f) (clone-on-set? #t))
  (unless (eq? (.ffi-type ffi-obj) '*)
    (error "extern-pointer can only be used with pointer ffi-objects" ffi-obj))
  (lambda (address)
    (define pointer (dereference-pointer address))
    (define (get)
      (decode ffi-obj pointer))
    (define (set value)
      (define new-pointer
	(let ((new-pointer* (encode ffi-obj value)))
	  (if clone-on-set?
	      (clone ffi-obj new-pointer*)
	    new-pointer*)))
      (when free-on-set?
	(free ffi-obj pointer))
      (c-memmove address (value-memory '* new-pointer) (sizeof '*)))
    (make-procedure-with-setter get set)))

(define-syntax-rule (define-extern (ffi-obj name) kw ...)
  (define name (false-if-exception
		((extern ffi-obj) (dynamic-pointer* 'name kw ...)))))

(define* (extern-pointer-array ffi-obj base-address array-size
			       #:key (free-on-set? #f) (clone-on-set? #t))
  (define (void*-offset count)
    (define offset (* count (sizeof '*)))
    (make-pointer (+ (pointer-address base-address) offset)))
  (define pointers (map void*-offset (iota array-size)))
  (define pointer->accessor
    (extern-pointer ffi-obj
		    #:free-on-set? free-on-set?
		    #:clone-on-set? clone-on-set?))
  (define variables (list->vector (map pointer->accessor pointers)))
  (lambda (n)
    (vector-ref variables n)))
