;;; decorate.scm ---

;; Copyright (C) 2014 Dmitry Bogatov <Dmitry Bogatov <KAction@gnu.org>>

;; Author: Dmitry Bogatov <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:


(define-module (ice-9 decorate)
  #:export (make-decorator cute)
  #:replace (cute))
(use-modules ((srfi srfi-26) #:prefix srfi-))

(define-syntax-rule
  (make-decorator id ((fn fn-args ...) dec-args ...) exp exp* ...)
  (lambda (fn dec-args ...)
    (define (decorated fn-args ...) exp exp* ...)
    (define fn-name (or (procedure-property fn 'name) 'unknown))
    (set-procedure-property! decorated 'name
                             (symbol-append fn-name '^ id))
    decorated))

(define-syntax cute
  (syntax-rules (<>)
    ((_ <> rest ...)
     (srfi-cute <> rest ...))
    ((_ fn rest ...)
     (let ((fixed (srfi-cute fn rest ...)))
       (define fn-name (or (procedure-property fn 'name) 'unknown))
       (define info-suffix
         (string->symbol
          (format #f "^<cute|~a:~a>"
                  (assq-ref (current-source-location) 'filename)
                  (and=> (assq-ref (current-source-location) 'line) 1+))))
       (set-procedure-property!
        fixed 'name (symbol-append fn-name info-suffix))
       fixed))))
